import { useState, useEffect, useContext } from 'react';
import { useNavigate, Navigate } from 'react-router-dom';
import { AuthContext } from '../../context/AuthContext';

import './style.css'

import Swal from 'sweetalert2';

export default function Register(){

	const navigate = useNavigate();

	const { currentUser } = useContext(AuthContext);

	const [ firstName, setFirstName ] = useState('');
	const [ lastName, setLastName ] = useState('');
	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');
	const [ confirmPassword, setConfirmPassword ] = useState('');

	const registerUser = async (e) => {
		e.preventDefault();
		// setFirstName('');
		// setLastName('');
		// setEmail('');
		// setPassword('');
		// setConfirmPassword('');

		try {
			const response = await fetch(`${process.env.REACT_APP_API_URL}/auth/register`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					firstName: firstName,
					lastName: lastName,
					email: email,
					password: password
				})
			});
			const user = await response.json();
			console.log(user);
			if(user === "Email already in use") {
				 Swal.fire({
                    title: "Duplicate email found",
                    icon: "error",
                    text: "Please provide a different email."
                });
			} else if(user.email === email) {
				Swal.fire({
                    title: "Registration successful",
                    icon: "success",
                    text: "Welcome to Zuitt."
                });
				navigate('/login')
			} else {
				Swal.fire({
                            title: "Something went wrong",
                            icon: "error",
                            text: "Please try again."
                });
			}
		} catch(error) {
			console.log(error);
		}
	};

	useEffect(() => {
		if ((firstName !== '' && lastName !== '' && email !== '' && password !== '' && confirmPassword) && (password === confirmPassword)) {
			document.querySelector('.btn-signup')?.removeAttribute('disabled');
		} else {
			document.querySelector('.btn-signup')?.setAttribute('disabled', true);
		}
	}, [firstName, lastName, email, password, confirmPassword]);

	return(
		(currentUser.id !== null) ?
		<Navigate to="/"/>
		:
		<>
			<div className="container">
				<div className="form-container">
					<form className="form" onSubmit={e => registerUser(e)}>
					<h1 className="text-signup">CREATE YOUR ACCOUNT</h1>
					<input
					 type="text" 
					 placeholder="first name"
					 value = {firstName}
					 onChange={e => setFirstName(e.target.value)}
					 required/>
					<input
					 type="text" 
					 placeholder="last name"
					 value = {lastName}
					 onChange={e => setLastName(e.target.value)}
					 required/>
					<input
					 type="email" 
					 placeholder="email"
					 value = {email}
					 onChange={e => setEmail(e.target.value)}
					 required/>
					<input
					 type="password" 
					 placeholder="password"
					 value = {password}
					 onChange={e => setPassword(e.target.value)}
					 required/>
					<input
					 type="password" 
					 placeholder="confirm password"
					 value = {confirmPassword}
					 onChange={e => setConfirmPassword(e.target.value)}
					 required/>
					<button className="btn btn-signup" disabled>SIGN UP</button>
					<div className="login-link-container">
						<p>Already have an account?</p>
						<p className="mini-link">Login</p>
					</div>
				</form>
				</div>
			</div>
		</>
	)
}