import { useState, useEffect, useContext } from 'react';

import AdminProductItem from '../AdminProductItem/AdminProductItem';

import { ProductContext } from '../../context/ProductContext';

import './style.css';

export default function AdminProductLists() {

	// const { allProducts, getAllProducts } = useContext(ProductContext);

	const [allProducts, setAllProducts] = useState([]);

	const getAllProducts = async () => {
		try {
			const response = await fetch(`${process.env.REACT_APP_API_URL}/products/`, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			const data = await response.json();
			console.log(data);
			setAllProducts(data);
		} catch(error) {
			console.log(error)
		}
	};

	useEffect(() => {
			getAllProducts();
	}, [])

	return (
		<>
			<table className="table-products-admin">
				<thead>
					<tr className="header-row-admin row-admin">
						<th>Name</th>
						<th>Description</th>
						<th>Brand</th>
						{/*<th>Category</th>*/}
						<th>Price</th>
						<th>Stock</th>
						<th>Status</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{
						allProducts && allProducts.map(product => {
							return (
								<AdminProductItem key={product._id} product={product}/>
							)
						})
					}
				</tbody>
			</table>		
		</>
	)
}