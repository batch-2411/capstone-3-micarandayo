
import './style.css';

export default function OrderItem({order, index}) {
	return (
		<>
			<div className="order-container">
				<div className="title-container-order">
					<h2 className="order-item-title">Order #{index + 1} - Purchased on: {order.purchasedOn.slice(0, 10)}</h2>
				</div>
				<div>
					<p className="items-text">Items</p>
					<ul className="item-list">
						{order.products.map(product => {
							return (
								<li key={product._id}>{product.name} - Quantity: {product.quantity}</li>
							)
						})}
					</ul>
					<p>Total: <span className="price-text-order">₱ {order.totalAmount}</span></p>
				</div>
			</div>
		</>
	)
}