import { useState, useEffect, useContext } from 'react';
import ProductCard from '../../components/ProductCard/ProductCard';
// import FeaturedProducts from '../../components/FeaturedProducts/FeaturedProducts';

import { ProductContext } from '../../context/ProductContext';

import './style.css'

export default function Products() {

	// const { activeProducts, featuredProducts, getActiveProducts, getFeaturedProducts } = useContext(ProductContext);
	const [activeProducts, setActiveProducts] = useState([]);
	const [featuredProducts, setFeaturedProducts] = useState([]);

	const getActiveProducts = async () => {
		try {
			const response = await fetch(`${process.env.REACT_APP_API_URL}/products/active`, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			const data = await response.json();
			console.log(data);
			setActiveProducts(data);
		} catch(error) {
			console.log(error)
		}
	};

	const getFeaturedProducts = async () => {
		try {
			const response = await fetch(`${process.env.REACT_APP_API_URL}/products/featured`, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			const data = await response.json();
			console.log(data);
			setFeaturedProducts(data);
		} catch(error) {
			console.log(error)
		}
	};

	useEffect(() => {
			getActiveProducts();
			getFeaturedProducts();
	}, [])

	return (
		<>
			<div className="container">
				<div className="products-container">
					<div className="featured-container">
						<div className="title-container">
							<i class="fa-solid fa-fire"></i>
							<h1>Featured Products</h1>
							<i class="fa-solid fa-fire"></i>
						</div>
						<div className="featured-products">
							{featuredProducts?.map(product => <ProductCard key={product._id} product={product}/>)}
						</div>
					</div>
					<div className="all-container">
						<div className="title-container">
							<h1>All Products</h1>
						</div>
						<div className="product-lists">
							{activeProducts?.map(product => <ProductCard key={product._id} product={product}/>)}
						</div>
					</div>
				</div>
			</div>
		</>
	)
}