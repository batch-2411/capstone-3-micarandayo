import './style.css';

export default function Home() {

	return (
		<>
			<div className="container">
				<div className="home-wrapper">
					<h1 className="welcome-text">Welcome to <span className="text-accent">PowerSpeed</span> Ecommerce Site</h1>
				</div>
			</div>
		</>
	)
}