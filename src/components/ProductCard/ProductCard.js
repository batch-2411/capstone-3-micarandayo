import { Link } from 'react-router-dom';

import './style.css';

export default function ProductCard({product}) {

	return (
		<>
			<Link to={`/products/${product._id}`}>
				<div className="card-wrapper">
					<div className="img-container">
						<img className="img" src={`/images/${product.image}`} alt=""/>
					</div>
					<div className="text-container">
						<h3 className="produc-name">{product.name}</h3>
						<p className="product-brand">{product.brand}</p>
						<p className="product-price">₱ {product.price}</p>
					</div>
				</div>
			</Link>
		</>
	)
}