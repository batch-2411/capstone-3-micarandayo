import { useState, useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import './style.css';

import { ProductContext } from '../../context/ProductContext';

export default function AdminProductItem({product}) {

	const { getAllProducts, getActiveProducts, getFeaturedProducts } = useContext(ProductContext);

	const [ isActive, setIsActive ] = useState(product.isActive)

	const disableProduct = async () => {
		try {
			const response = await fetch(`${process.env.REACT_APP_API_URL}/products/${product._id}/archive`, {
				method: 'PATCH',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					isActive: !isActive
				})
			})
			if(response.status === 200) {
				const data = await response.json();
				setIsActive(data.isActive);
				getActiveProducts();
				getAllProducts();
				getFeaturedProducts();
			}
		} catch(error) {
			console.log(error);
		}

	}
	return (
		<>
		<tr className="row-admin">
			<td>{product.name}</td>
			<td>{product.description}</td>
			<td>{product.brand}</td>
			<td>{product.price}</td>
			<td>{product.countInStock}</td>
			<td>{isActive ? 'Active' : 'Inactive'}</td>
			<td>
				<Link to={`/updateProduct/${product._id}`}>
					<button className="btn btn-update-admin-item">Update</button>
				</Link>
				<button className={isActive ? 'btn btn-disable-admin-item' : 'btn btn-activate-admin-item'} onClick={disableProduct}>{isActive ? 'Disable' : 'Enable'}</button>
			</td>
		</tr>
		</>
		)
}