import { useState, useEffect } from 'react';

import './style.css';

export default function AdminUsersOrders() {

	const [ orders, setOrders ] = useState([]);
	const [ users, setUsers ] = useState([]);

	const getAllOrders = async () => {
		try {
			const response = await fetch(`${process.env.REACT_APP_API_URL}/orders/all-orders`, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			});
			if(response.status === 200) {
				const data = await response.json();
				setOrders(data);
				console.log(data);
				let users = [];
				 data.map(order => {
				 	if(users.length === 0) {
				 		users.push({
				 			userEmail: order.userEmail,
				 			order:[
				 				{
				 				purchasedOn: order.purchasedOn,
				 				totalAmount: order.totalAmount,
				 				products:
				 					order.products.map(product => {
				 						return {
				 							name: product.name,
				 							quantity: product.quantity
				 						}
				 					})
				 			}
				 			]
				 		})
				 	} else {
				 		const found = users.some(user => user.userEmail === order.userEmail)
				 		console.log(found)
				 		if(found) {
				 			const index = users.findIndex(user => user.userEmail === order.userEmail);
				 			users[index].order.push({
				 			purchasedOn: order.purchasedOn,
				 			totalAmount: order.totalAmount,
				 			products:
				 					order.products.map(product => {
				 						return {
				 							name: product.name,
				 							quantity: product.quantity
				 						}
				 					})

				 			})
				 		} else {
				 			users.push({
				 			userEmail: order.userEmail,
				 			order:[
				 				{
				 				purchasedOn: order.purchasedOn,
				 				totalAmount: order.totalAmount,
				 				products:
				 					order.products.map(product => {
				 						return {
				 							name: product.name,
				 							quantity: product.quantity
				 						}
				 					})
				 			}
				 			]
				 		})
				 		}
				 	}
				});
				 console.log(users)
				 setUsers(users);
			}
		} catch(error) {
			console.log(error);
		}
	};



	useEffect(() => {
		getAllOrders();
	}, [])

	return (
		<>
			<div className="container">
				<div className="orders-container-admin">
					<h2 className="orders-title-admin">Order History</h2>
					<div className="order-wrapper">
						{
							users.map(user => {
								return (
									<div key={user.userEmail} className="order-item-admin">
										<h3 className="order-title-admin">Orders for user {user.userEmail}</h3>
										{
											user.order.map((order, index) => {
											return (
												<div className="order-per-purchase" key={index}>
													<h4 className="order-date-admin">Purchased on {order.purchasedOn.slice(0, 10)}</h4>
													{
														order.products.map((product, index) => {
															return (
																<div key = {index}>
																	<p className="order-product-admin">{product.name} - Quantity: {product.quantity}</p>
																</div>
															)
														})
													}
													<h5 className="order-total-admin">Total: <span>₱ {order.totalAmount}</span></h5>
												</div>
											)
										})
										}
									</div>
								)
							})
						}
					</div>
				</div>
			</div>
		</>
	)
}