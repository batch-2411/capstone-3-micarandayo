import './App.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import NavBar from './components/NavBar/NavBar';
import Cart from './components/Cart/Cart';
import Home from './pages/Home/Home';
import Register from './pages/Register/Register';
import Login from './pages/Login/Login';
import Products from './pages/Products/Products';
import Orders from './pages/Orders/Orders';
import Users from './pages/Users/Users';
import Profile from './pages/Profile/Profile';
import AdminDashboard from './pages/AdminDashboard/AdminDashboard';
import AddProduct from './components/AddProduct/AddProduct';
import UpdateProduct from './components/UpdateProduct/UpdateProduct';
import ProductView from './components/ProductView/ProductView';


function App() {

  

  return (
   <>
     <Router>
      <NavBar/>
      <Cart/>
       <Routes>
         <Route path='/' element={<Home/>}/>
         <Route path='/register' element={<Register/>}/>
         <Route path='/login' element={<Login/>}/>
         <Route path='/products' element={<Products/>}/>
         <Route path='/orders' element={<Orders/>}/>
         <Route path='/users' element={<Users/>}/>
         <Route path='/profile' element={<Profile/>}/>
         <Route path='/products/:productId' element={<ProductView/>}/>
         <Route path='/adminDashboard' element={<AdminDashboard/>}/>
         <Route path='/addProduct' element={<AddProduct/>}/>
         <Route path='/updateProduct/:productId' element={<UpdateProduct/>}/>
       </Routes>
     </Router>
   </>
  );
}

export default App;
