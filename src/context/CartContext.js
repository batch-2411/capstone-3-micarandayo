import { createContext, useState, useEffect } from 'react';

export const CartContext = createContext();
	
export const CartContextProvider = ({children}) => {

	const [ cart, setCart ] = useState({});
	const [ isCartOpen, setIsCartOpen ] = useState(false);


	const [ cartItems, setCartItems ] = useState([]);

	// const getCartProducts = (data) => {
	// }

	const retrieveCart = async () => {
		try {
			const response = await fetch(`${process.env.REACT_APP_API_URL}/carts/`, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			if(response.status === 200) {
				const data = await response.json();
				console.log(data);
				setCart(data);
				const allProducts = data?.products.map(product => {
						return {
							productId: product.productId,
							name: product.name,
							quantity: product.quantity
						}
					})
				setCartItems(allProducts);
				}
		} catch(error) {
			console.log(error);
		}
	};

	const removeProduct = async (productName) => {
		console.log(productName)
		try {
			const response = await fetch(`${process.env.REACT_APP_API_URL}/carts/remove-product`, {
				method: 'DELETE',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					productName: productName
				})
			})
			if(response.status === 200) {
				const data = await response.json();
				if(data.acknowledged === true) {
					setCart({});
					console.log(data.acknowledged);
					setCartItems([]);
				} else {
					setCart(data);
				}
			}
		} catch(error) {
			console.log(error);
		}
	};

	useEffect(() => {
		retrieveCart();
		// getCartProducts();
	}, [])

	return (
		<CartContext.Provider value = {{cart, isCartOpen, setIsCartOpen, cartItems, retrieveCart, removeProduct}}>
			{children}
		</CartContext.Provider>
	)
}
