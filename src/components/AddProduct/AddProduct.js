import { useState, useContext } from 'react';
import { Link } from 'react-router-dom';
import { ProductContext } from '../../context/ProductContext';

import Swal from 'sweetalert2';

import './style.css';

export default function AddProduct() {

	const { getActiveProducts } = useContext(ProductContext);

	const [ name, setName ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ brand, setBrand ] = useState('');
	const [ price, setPrice ] = useState('');
	const [ countInStock, setCountInStock ] = useState('');
	const [ fileData, setFileData ] = useState('');
	const [ image, setImage] = useState('');

	const handleFileChange = ({target}) => {
		console.log(target.value)
		console.log(target.files[0].name)
		setFileData(target.files[0].name);
		setImage(target.value);
	}

	const addProduct = async (e) => {
		e.preventDefault();
		try {
			const response = await fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					name: name,
					description: description,
					image: fileData,
					brand: brand,
					price: price,
					countInStock: countInStock
				})
			});
			if(response.status === 201) {
				getActiveProducts();
				Swal.fire({
					title: "Add Successful",
					icon: "success",
					text: `${name} successfully created!`
				});

				setName('');
				setDescription('');
				setBrand('');
				setPrice('');
				setCountInStock('');
			} else {
				Swal.fire({
					title: "Add Unsuccessful",
					icon: "error",
					text: "Please fill all fields"
				});
			}
			console.log(fileData);
			console.log(image);
		} catch(error) {
			console.log(error)
		}
	}

	return (
		<>
			<div className="container">
				<div className="add-container">
					<form className="add-form" onSubmit={e => addProduct(e)}>
						<h1 className="title-add-product">Add Product</h1>
						<div className="box-input">
							<label htmlFor="">Name</label>
							<input 
							type="text" 
							placeholder="Product Name"
							value={name}
							required
							onChange={e => setName(e.target.value)}/>
						</div>
						<div className="box-input">
							<label htmlFor="">Description</label>
							<input 
							type="text" 
							placeholder="Product Description"
							value={description}
							required
							onChange={e => setDescription(e.target.value)}/>
						</div>
						<div className="box-input">
							<label htmlFor="image">Image</label>
							<input 
							type="file"
							name="image"
							accept="image/*"
							placeholder="Upload Image"
							required
							onChange={handleFileChange}/>
						</div>
						<div className="box-input">
							<label htmlFor="">Brand</label>
							<input 
							type="text" 
							placeholder="Product Brand"
							value={brand}
							required
							onChange={e => setBrand(e.target.value)}/>
						</div>
						<div className="box-input">
							<label htmlFor="">Price</label>
							<input 
							type="number" 
							placeholder="Product Price"
							value={price}
							required
							onChange={e => setPrice(e.target.value)}/>
						</div>
						<div className="box-input">
							<label htmlFor="">In Stock</label>
							<input 
							type="number" 
							placeholder="Product Stocks"
							value={countInStock}
							required
							onChange={e => setCountInStock(e.target.value)}/>
						</div>
						<div className="add-product-btn-contnainer">
							<button className="btn btn-add-product">Add</button>
							<Link className="btn-add-product-cancel-container" to={"/adminDashboard"}>
								<button className="btn btn-add-product-cancel">Cancel</button>
							</Link>
						</div>
					</form>
				</div>
			</div>
		</>
	)
}