import { useState, useContext } from 'react';
import { NavLink, useNavigate } from 'react-router-dom';

import { AuthContext } from '../../context/AuthContext';
import { CartContext } from '../../context/CartContext';

import './style.css'

export default function NavBar() {

	let activeStyle = {
    textDecoration: "underline",
  };

	const navigate = useNavigate();

	const { currentUser, unsetCurrentUser } = useContext(AuthContext);
	const { isCartOpen, setIsCartOpen } = useContext(CartContext);

	const [ isOpen, setIsOpen ] = useState(false);

	const logoutUser = () => {
		navigate('/login');
		unsetCurrentUser();
		localStorage.clear();
	}

	return (
		<>
			<div className="nav-container">
				<nav className="container navbar-wrapper">
					<NavLink to="/" className="logo">PowerSpeed</NavLink>
					<ul className="main-links">
						<NavLink style={({ isActive }) =>
				              isActive ? activeStyle : undefined
				            } to="/">Home</NavLink>
						<NavLink style={({ isActive }) =>
				              isActive ? activeStyle : undefined
				            } to="/products">Products</NavLink>
						{ (currentUser.id !== null && !currentUser.isAdmin) && <NavLink style={({ isActive }) =>
				              isActive ? activeStyle : undefined
				            } to="/orders">Orders</NavLink>}
						{ currentUser.isAdmin && 
						<>
							<NavLink style={({ isActive }) =>
				              isActive ? activeStyle : undefined
				            } to="/users">Users</NavLink>
							<NavLink style={({ isActive }) =>
				              isActive ? activeStyle : undefined
				            } to="/adminDashboard">Dashboard</NavLink>
						</> }
					</ul>
					<ul className="sub-links">
						{
							(currentUser.id !== null) ?
							<>
								<div className="btn-profile" onClick={() => setIsOpen(!isOpen)}>
									<i className="fa-solid fa-user"></i>
									{isOpen && (
										<div className="profile-dropdown">
										<div className="dropdown-container">
											<div className="btn-details" onClick={() => navigate('/profile')}>
												<i className="fa-solid fa-user"></i>
												<span>My Profile</span>
											</div>
											<div className="btn-logout" onClick={logoutUser}>
												<i className="fa-solid fa-right-from-bracket"></i>
												<span>Logout</span>
											</div>
										</div>
									</div>
									)}
								</div>
								{ !currentUser.isAdmin &&
								<div className="btn-cart" onClick={() => {setIsCartOpen(!isCartOpen)}}>
									<i className="fa-solid fa-cart-shopping"></i>
								</div>}
								{/*<button className="btn btn-logout" onClick={logoutUser}>Logout</button>*/}
							</>
							:
							<>
								<NavLink style={({ isActive }) =>
						              isActive ? activeStyle : undefined
						            } to="/login">Login</NavLink>
								<NavLink style={({ isActive }) =>
						              isActive ? activeStyle : undefined
						            } to="/register">Sign Up</NavLink>
													</>
						}
					</ul>
				</nav>
			</div>
		</>
	)
}