import { createContext, useState, useEffect } from 'react';

export const ProductContext = createContext();
	
export const ProductContextProvider = ({children}) => {

	const [allProducts, setAllProducts] = useState([]);
	const [activeProducts, setActiveProducts] = useState([]);
	const [featuredProducts, setFeaturedProducts] = useState([]);

	const getAllProducts = async () => {
		try {
			const response = await fetch(`${process.env.REACT_APP_API_URL}/products/`, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			const data = await response.json();
			console.log(data);
			setAllProducts(data);
		} catch(error) {
			console.log(error)
		}
	};

	const getActiveProducts = async () => {
		try {
			const response = await fetch(`${process.env.REACT_APP_API_URL}/products/active`, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			const data = await response.json();
			console.log(data);
			setActiveProducts(data);
		} catch(error) {
			console.log(error)
		}
	};

	const getFeaturedProducts = async () => {
		try {
			const response = await fetch(`${process.env.REACT_APP_API_URL}/products/featured`, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			const data = await response.json();
			console.log(data);
			setFeaturedProducts(data);
		} catch(error) {
			console.log(error)
		}
	};

	useEffect(() => {
			getAllProducts();
			getActiveProducts();
			getFeaturedProducts();
	}, [])

	return (
		<ProductContext.Provider value = {{allProducts, activeProducts, featuredProducts, getActiveProducts, getAllProducts, getFeaturedProducts}}>
			{children}
		</ProductContext.Provider>
	)
}
