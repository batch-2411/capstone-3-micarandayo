import { useState } from 'react';
import AdminProductLists from '../../components/AdminProductLists/AdminProductLists';
import AdminUsersOrders from '../../components/AdminUsersOrders/AdminUsersOrders';
import { NavLink } from 'react-router-dom';

import './style.css'

export default function AdminDashboard() {

	const [ isOrder, setIsOrder ] = useState(false);

	return (
		<>
			<div className="container">
				<div className="admin-wrapper">
					<div className="heading-container">
					<h1 className="title-admin">Admin Dashboard</h1>
					<div className="admin-btn-container">
						<NavLink to="/addProduct" className="btn btn-add-product">Add New Product</NavLink>
						{
							isOrder ?
							<button className="btn btn-show-products" onClick={() => setIsOrder(!isOrder)}>Show All Products</button>
							:
							<button className="btn btn-show-orders" onClick={() => setIsOrder(!isOrder)}>Show User Orders</button>
						}
						
					</div>
					</div>
					{	isOrder ?
						<AdminUsersOrders/>
						:
						<AdminProductLists/>
					}
				</div>
			</div>
		</>
	)
}