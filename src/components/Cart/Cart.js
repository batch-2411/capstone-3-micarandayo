import { useEffect, useContext } from 'react';
import { useNavigate } from 'react-router-dom';

import { AuthContext } from '../../context/AuthContext';
import { CartContext } from '../../context/CartContext';

import Swal from 'sweetalert2';

import './style.css';

export default function Cart() {

	const navigate = useNavigate();

	const { currentUser } = useContext(AuthContext);

	const { cart, isCartOpen, retrieveCart, removeProduct, cartItems } = useContext(CartContext);


	const checkOutCart = async () => {
		if(localStorage.getItem('token') !== null) {
			try {
				Swal.fire({
				  title: `Do you want to check out the items in the cart?`,
				  showDenyButton: true,
				  confirmButtonText: 'Yes',
				  denyButtonText: 'No',
				}).then(async (result) => {
				  if (result.isConfirmed) {
				    const response = await fetch(`${process.env.REACT_APP_API_URL}/orders`, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						Authorization: `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
						products: [
							...cartItems
						]
					})
						})
						if(response.status === 201) {
							Swal.fire({
							title: "Checkout Successful",
			                icon: "success",
			                text: `Cart items has been successfully checked out`
						})
						retrieveCart();
						navigate('/orders')
						}
				  } else if (result.isDenied) {
				    Swal.fire('Checkout cancelled')
				  }
				})
				
			} catch(error) {
				console.log(cartItems)
				console.log(error);
			}
		} else {
			Swal.fire({
				title: "Please login",
                icon: "error",
                text: "You have to login first."
			})
		}
	};

	const increaseQuantity = async (productName, quantity) => {
		try {
			const response = await fetch(`${process.env.REACT_APP_API_URL}/carts/change-quantity`, {
				method: 'PATCH',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					productName: productName,
					quantity: (quantity + 1)
				})
			})
			retrieveCart();
		} catch(error) {
			console.log(error);
		}
	};

	const reduceQuantity = async (productName, quantity) => {
		try {
			const response = await fetch(`${process.env.REACT_APP_API_URL}/carts/change-quantity`, {
				method: 'PATCH',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					productName: productName,
					quantity: (quantity - 1)
				})
			})
			retrieveCart();
		} catch(error) {
			console.log(error);
		}
	};

	useEffect(() => {
		retrieveCart();
	}, []);
	console.log(cartItems)
	return (
		<>
			{(currentUser.isAdmin === false) && 
			<div className={isCartOpen ? 'main-container' : 'main-container cart-hidden'}>
				<div className="cart-container">
					<h1 className="title-text-cart">Shopping Cart</h1>
					{
						(!cart || cartItems.length === 0) ?
						<div>
							<h3 className="text-notice">No products in the cart</h3>
						</div>
						:
						<table className="cart-table">
						<thead className="thead-container">
							<tr>
								<th>
									Product
								</th>
								<th>
									Price
								</th>
								<th>
									Qty.
								</th>
								<th>
									Subtotal
								</th>
								<th>
									Actions
								</th>
							</tr>
						</thead>
						<tbody className="tbody-container">
							{
								cart?.products?.map(product => {
									return (
										<tr key={product._id} className="">
											<td>
												<span>{product.name}</span>
											</td>
											<td>
												<span>{product.price}</span>
											</td>
											<td className="quantity-wrapper">
												<button className="btn btn-reduce-quantity-cart" onClick={() => reduceQuantity(product.name, product.quantity)}>-</button>
												<span>{product.quantity}</span>
												<button className="btn btn-add-quantity-cart" onClick={() => increaseQuantity(product.name, product.quantity)}>+</button>
											</td>
											<td>
												<span className="subTotal">{product.subTotal}</span>
											</td>
											<td>
												<button className="btn btn-remove-item" onClick={() => {removeProduct(product.name)}}>
													<i className="fa-solid fa-xmark"></i>
												</button>
											</td>
										</tr>
									)
								})
							}
							{/*<div className></div>*/}
							<tr className="total-container">
											<td>
												
											</td>
											<td>
												
											</td>
											<td>
												Total:
											</td>
											<td>
												<span className="total-cart">₱ {cart?.total}</span>
											</td>
											<td>
												<button className="btn btn-checkout-cart" onClick={checkOutCart}>
													<span>Checkout</span>
												</button>
											</td>
										</tr>
						</tbody>
					</table>
					}
				</div>
			</div>}
		</>
	)
}