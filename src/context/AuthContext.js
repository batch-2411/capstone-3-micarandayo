import { createContext, useState, useEffect } from 'react';

export const AuthContext = createContext();
	
export const AuthContextProvider = ({children}) => {

	const [ currentUser, setCurrentUser ] = useState({
		id: null,
		email: null,
		isAdmin: null
	});

	const unsetCurrentUser = () => {
		setCurrentUser({
			id: null,
			email: null,
			isAdmin: null
		});
	};

	const setUser = async (token) => {
		try {
			const response = await fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
			});
			const data = await response.json();
			console.log(data)
			setCurrentUser({
				id: data.id,
				email: data.email,
				isAdmin: data.isAdmin
			})

		} catch(error) {
			console.log(error)
		}
	};

	const getCurrentUser = async () => {
		try {
			const response = await fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
			});
			if(response.status === 200) {
				const data = await response.json();
				console.log(data);
				setCurrentUser(data);
			} else {
				setCurrentUser({
					id: null,
					email: null,
					isAdmin: null
				})
			}
			console.log(response);
		} catch(error) {
			console.log(error);
		}
	};

	useEffect(() => {
		getCurrentUser();
	}, []);

	return (
		<AuthContext.Provider value = {{currentUser, setUser, unsetCurrentUser}}>
			{children}
		</AuthContext.Provider>
	)
}
