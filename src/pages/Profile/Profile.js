import { useState, useEffect } from 'react';
import './style.css';

import Swal from 'sweetalert2';

export default function Profile() {

	const [ user, setUser ] = useState({});

	const [ firstName, setFirstName ] = useState('');
	const [ lastName, setLastName ] = useState('');
	const [ email, setEmail ] = useState('');
	const [ mobileNo, setMobileNo ] = useState('');
	const [ password, setPassword ] = useState('');

	const getUserDetails = async () => {
		try {
			const response = await fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
			});
			if(response.status === 200) {
				const data = await response.json();
				setUser(data);
				setFirstName(data.firstName);
				setLastName(data.lastName);
				setEmail(data.email);
				setMobileNo(data.mobileNo);
			}
		} catch(error) {
			console.log(error);
		}
	}

	const udpateProfile = async (e) => {
		e.preventDefault();
		let userData = {}
		if(password !== '') {
			userData = {
					firstName: firstName,
					lastName: lastName,
					email: email,
					mobileNo: mobileNo,
					password: password
			}
		} else {
			userData = {
					firstName: firstName,
					lastName: lastName,
					email: email,
					mobileNo: mobileNo
			}
		}
		try {
			Swal.fire({
				  title: `Do you want to update your profile?`,
				  showDenyButton: true,
				  confirmButtonText: 'Yes',
				  denyButtonText: 'No',
				}).then(async (result) => {
				  if (result.isConfirmed) {
				    const response = await fetch(`${process.env.REACT_APP_API_URL}/users`, {
						method: 'PUT',
						headers: {
							'Content-Type': 'application/json',
							Authorization: `Bearer ${localStorage.getItem('token')}`
						},
						body: JSON.stringify(userData)
					});
						if(response.status === 200) {
							Swal.fire({
							title: "Update Successful",
			                icon: "success",
			                text: `Your profile has been successfully updated`
						})
						}
						setPassword('')
						getUserDetails();
				  } else if (result.isDenied) {
				    Swal.fire('Update cancelled')
				  }
				})
			} catch(error) {
			console.log(error);
		}
	}

	useEffect(() => {
		getUserDetails();
	}, [])

	return (
		<>
			<div className="container">
				<div>
					<div className="content-wrapper">
						<div className="left-container">
							<div className="img-container img-profile">
								<img className="img" src="/images/prof2.jpg" alt=""/>
							</div>
							<p className="first-name">{user.firstName}</p>
							<p>{user.email}</p>
						</div>
						<div className="right-container">
							<h2 className="title-text-profile">Profile Settings</h2>
								<form className="form" onSubmit={e => udpateProfile(e)}>
									<div className="box-input">
										<label htmlFor="">First Name</label>
										<input 
										type="text" 
										placeholder="First Name"
										value={firstName}
										onChange={e => setFirstName(e.target.value)}/>
									</div>
									<div className="box-input">
										<label htmlFor="">Last Name</label>
										<input 
										type="text" 
										placeholder="Last Name"
										value={lastName}
										onChange={e => setLastName(e.target.value)}/>
									</div>
									<div className="box-input">
										<label htmlFor="">Email</label>
										<input 
										type="email" 
										placeholder="Email"
										value={email}
										onChange={e => setEmail(e.target.value)}/>
									</div>
									<div className="box-input">
										<label htmlFor="">Mobile No.</label>
										<input 
										type="text" 
										placeholder="Mobile No."
										value={email}
										onChange={e => setMobileNo(e.target.value)}/>
									</div>
									<div className="box-input">
										<label htmlFor="">Password</label>
										<input 
										type="password" 
										placeholder="******"
										value={password}
										onChange={e => setPassword(e.target.value)}/>
									</div>
									<button className="btn btn-save-profile" onClick={udpateProfile}>Save Profile</button>
								</form>
						</div>
					</div>
				</div>
			</div>
		</>
	)
}