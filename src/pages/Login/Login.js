import { useState, useEffect, useContext } from 'react';
import {  Navigate } from 'react-router-dom';
import { AuthContext } from '../../context/AuthContext';

import './style.css'

import Swal from 'sweetalert2';

export default function Register(){

	const { currentUser, setUser } = useContext(AuthContext);

	const [ firstName, setFirstName ] = useState('');
	const [ lastName, setLastName ] = useState('');
	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');
	const [ confirmPassword, setConfirmPassword ] = useState('');

	const loginUser = async (e) => {
		e.preventDefault();

		try {
			const response = await fetch(`${process.env.REACT_APP_API_URL}/auth/login`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					email: email,
					password: password
				})
			});
			console.log(response)
			const data = await response.json();
			console.log(data);
			if(data === "Incorrect Password") {
				 Swal.fire({
                    title: "Incorrect password",
                    icon: "error",
                    text: "Please enter correct password."
                });
				 
			} else if(data.message === "Successfully logged in!") {
				Swal.fire({
                    title: "Login successful",
                    icon: "success",
                    text: "Welcome!"
                });
                localStorage.setItem('token', data.access);
                setUser(data.access);
			} else {
				Swal.fire({
                            title: "Invalid Credentials",
                            icon: "error",
                            text: "Please try again."
                });
			}
		} catch(error) {
			console.log(error);
		}
	};

	// useEffect(() => {
	// 	if (email !== '' && password !== '') {
	// 		document.querySelector('.btn-login').removeAttribute('disabled');
	// 	} else {
	// 		document.querySelector('.btn-login').setAttribute('disabled', true);
	// 	}
	// }, [email, password])

	return(
		(currentUser.id !== null) ?
		<Navigate to="/"/>
		:
		<>
			<div className="container">
				<div className="form-container">
					<form className="form" onSubmit={e => loginUser(e)}>
					<h1 className="text-login">WELCOME</h1>
					<input
					 type="email" 
					 placeholder="email"
					 value = {email}
					 onChange={e => setEmail(e.target.value)}
					 required/>
					<input
					 type="password" 
					 placeholder="password"
					 value = {password}
					 onChange={e => setPassword(e.target.value)}
					 required/>
					<button className="btn btn-login secondary-font ">LOGIN</button>
					<div className="sign-up-link-container">
						<p>Need an account?</p>
						<p className="mini-link">Sign Up</p>
					</div>
				</form>
				</div>
			</div>
		</>
	)
}