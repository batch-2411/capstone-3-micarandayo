import { useState, useEffect } from 'react';

import './style.css';

export default function Users() {

	const [ users, setUsers ] = useState([]);
	const [ isAdmin, setIsAdmin ] = useState();

	const getAllUsers = async () => {
		try {
			const response = await fetch(`${process.env.REACT_APP_API_URL}/users`, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			if(response.status === 200) {
				const data = await response.json();
				console.log(data)
				setUsers(data);
			} else {
				console.log('Something went wrong');
			}
		} catch(error) {
			console.log(error);
		}
	};

	const setUserAdmin = async (userId) => {
		try {
			const response = await fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/set-admin`, {
				method: 'PATCH',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					isAdmin: !isAdmin
				})
			})
			if(response.status === 200) {
				const data = await response.json();
				setIsAdmin(data.isAdmin);
				getAllUsers();
			}
		} catch(error) {
			console.log(error);
		}

	}

	useEffect(() => {
		getAllUsers();
	}, []);

	return (
		<>
			<div className="container">
				<div className="users-wrapper">
					<h2 className="title-users">Users</h2>
					<table className="table-users">
						<thead>
							<tr className="header-row-users row-users">
								<th>Name</th>
								<th>Email</th>
								<th>Role</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							{
								users?.map(user => {
									return (
										<tr className="row-users" key={user._id}>
											<td>{`${user.firstName} ${user.lastName}`}</td>
											<td>{user.email}</td>
											<td>{user.isAdmin ? 'Admin' : 'User'}</td>
											<td>
												<button onClick={() => setUserAdmin(user._id)} className={user.isAdmin ? 'btn btn-disable-users' : 'btn btn-activate-users'}>{user.isAdmin ? 'Demote' : 'Promote'}</button>
											</td>
										</tr>
									)
								})
							}
						</tbody>
					</table>
				</div>
			</div>
		</>
	)
}