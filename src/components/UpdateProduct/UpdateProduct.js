import { useState, useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';

import Swal from 'sweetalert2';

import './style.css';

export default function UpdateProduct() {

	const {productId} = useParams();

	const [ name, setName ] = useState('');
	const [ description, setDescription ] = useState('');
	// const [ category, setCategory ] = useState('');
	const [ brand, setBrand ] = useState('');
	const [ price, setPrice ] = useState('');
	const [ countInStock, setCountInStock ] = useState('');

	const retrieveProduct = async () => {
		try {
			const response = await fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
			if(response.status === 200) {
				const product = await response.json();
				console.log(product);
				setName(product.name);
				setDescription(product.description);
				setBrand(product.brand);
				setPrice(product.price);
				setCountInStock(product.countInStock);
			}
		} catch(error) {
			console.log(error);
		}
	}

	useEffect(() => {
		retrieveProduct();
	}, []);


	const updateProduct = async (e) => {
		e.preventDefault();
		try {
			const response = await fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					name: name,
					description: description,
					brand: brand,
					price: price,
					countInStock: countInStock
				})
			});
			if(response.status === 200) {
				Swal.fire({
					title: "Update Successful",
					icon: "success",
					text: `${name} successfully created!`
				});
			} else {
				Swal.fire({
					title: "Update Unsuccessful",
					icon: "error",
					text: "Please fill all fields"
				});
			}
		} catch(error) {
			console.log(error)
		}
	}

	return (
		<>
			<div className="container">
				<div className="add-container">
					<form onSubmit={e => updateProduct(e)}>
						<h1 className="title">Update Product</h1>
						<div className="box-input">
							<label htmlFor="">Name</label>
							<input 
							type="text" 
							placeholder="Product Name"
							value={name}
							onChange={e => setName(e.target.value)}/>
						</div>
						<div className="box-input">
							<label htmlFor="">Description</label>
							<input 
							type="text" 
							placeholder="Product Description"
							value={description}
							onChange={e => setDescription(e.target.value)}/>
						</div>
{/*						<div className="box-input">
							<label htmlFor="">Category</label>
							<select name="category" id="category">
								<option value="accessories">Accessories</option>
								<option value="bodyFrame">Body & Frame</option>
								<option value="exhaust">Exhausts & Exhaust Systems</option>
								<option value="seating">Seating</option>
								<option value="tiresTubes">Wheels, Tires & Tubes</option>
							</select>
						</div>*/}
						<div className="box-input">
							<label htmlFor="">Brand</label>
							<input 
							type="text" 
							placeholder="Product Brand"
							value={brand}
							onChange={e => setBrand(e.target.value)}/>
						</div>
						<div className="box-input">
							<label htmlFor="">Price</label>
							<input 
							type="number" 
							placeholder="Product Price"
							value={price}
							onChange={e => setPrice(e.target.value)}/>
						</div>
						<div className="box-input">
							<label htmlFor="">In Stock</label>
							<input 
							type="number" 
							placeholder="Product Stocks"
							value={countInStock}
							onChange={e => setCountInStock(e.target.value)}/>
						</div>
						<button className="btn btn-update-product">Update</button>
						<Link to={"/adminDashboard"}>
							<button className="btn btn-cancel">Cancel</button>
						</Link>
					</form>
				</div>
			</div>
		</>
	)
}