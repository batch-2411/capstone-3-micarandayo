import { useState, useEffect } from 'react';
import OrderItem from '../../components/OrderItem/OrderItem';

import './style.css';


export default function Orders() {

	const [ orders, setOrders ] = useState([]);

	const retrieveOrders = async () => {
		try {
			const response = await fetch(`${process.env.REACT_APP_API_URL}/orders/user-orders`, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			if(response.status === 200) {
				const data = await response.json();
				setOrders(data);
				console.log(data);
			}
		} catch(error) {
			console.log(error);
		}
	}

	useEffect(() => {
		retrieveOrders();
	}, [])

	return (
		<>
			<div className="container">
				<div className="order-wrapper">
					<h1 className="title-order">Order History</h1>
					{orders?.map((order, index) => {
						return (
							<OrderItem key={order._id} order={order} index={index}/>
						)
					})}
				</div>
			</div>
		</>
	)
}