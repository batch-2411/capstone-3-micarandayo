import { useState, useEffect, useContext } from 'react';
import { useParams } from 'react-router-dom';
import { CartContext } from '../../context/CartContext';
import { AuthContext } from '../../context/AuthContext';

import Swal from 'sweetalert2';

import './style.css';

export default function ProductView() {

	const { currentUser } = useContext(AuthContext);
	const { retrieveCart } = useContext(CartContext);

	const { productId } = useParams();

	const [ product, setProduct ] = useState({});
	const [ quantity, setQuantity ] = useState(1);

	const getProductDetails = async () => {
		try {
			const response = await fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
			if(response.status === 200) {
				const data = await response.json();
				setProduct(data);
			}
		} catch(error) {
			console.log(error);
		}
	}

	const checkOut = async () => {
		if(localStorage.getItem('token') !== null) {
			try {
				Swal.fire({
				  title: `Do you want to check out ${product.name}?`,
				  showDenyButton: true,
				  confirmButtonText: 'Yes',
				  denyButtonText: 'No',
				}).then(async (result) => {
				  if (result.isConfirmed) {
				    const response = await fetch(`${process.env.REACT_APP_API_URL}/orders`, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						Authorization: `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
						products: [
							{
								productId: productId,
								name: product.name,
								quantity: quantity
							}
						]
					})
						})
						if(response.status === 201) {
							Swal.fire({
							title: "Checkout Successful",
			                icon: "success",
			                text: `${product.name} has been successfully checked out`
						})
						}
				  } else if (result.isDenied) {
				    Swal.fire('Checkout cancelled')
				  }
				})
				
			} catch(error) {
				console.log(error);
			}
		} else {
			Swal.fire({
				title: "Please login",
                icon: "error",
                text: "You have to login first."
			})
		}
	};

	const addToCart = async () => {
		if(localStorage.getItem('token') !== null) {
			try {
				Swal.fire({
				  title: `Do you want to add to cart ${product.name}?`,
				  showDenyButton: true,
				  confirmButtonText: 'Yes',
				  denyButtonText: 'No',
				}).then(async (result) => {
				  if (result.isConfirmed) {
				    const response = await fetch(`${process.env.REACT_APP_API_URL}/carts/add-product`, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						Authorization: `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
							productId: productId,
							quantity: quantity
						})
						})
						if(response.status === 201) {
							Swal.fire({
							title: "Add to Cart Successful",
			                icon: "success",
			                text: `${product.name} has been successfully added to cart`
						})
							retrieveCart();
						}
				  } else if (result.isDenied) {
				    Swal.fire('Add to Cart cancelled')
				  }
				})
				
			} catch(error) {
				console.log(error);
			}
		} else {
			Swal.fire({
				title: "Please login",
                icon: "error",
                text: "You have to login first."
			})
		}
	};

	useEffect(() => {
		getProductDetails();
	}, [productId]);
	
	useEffect(() => {
		console.log(currentUser.isAdmin)
		if(currentUser.isAdmin) {
			document.querySelector('.btn-buy-now').setAttribute('disabled', true);
			document.querySelector('.btn-add-to-cart').setAttribute('disabled', true);
		}
	}, [currentUser]);

	useEffect(() => {
		console.log(quantity)
		if(quantity === 1) {
			document.querySelector('.btn-reduce-quantity').setAttribute('disabled', true);
		} else {
			document.querySelector('.btn-reduce-quantity').removeAttribute('disabled');
		}
	}, [quantity])

	return (
		<>
			<div className="container">
				<div className="item-container">
					<div className="item-img-wrapper">
						<img className="img" src={`/images/${product.image}`}/>
					</div>
					<div className="item-content-wrapper">
						<h2 className="product-title">{product.name}</h2>
						<p>{product.description}</p>
						<p>Brand: <span>{product.brand}</span></p>
						<p className="price">₱ {product.price}</p>
						<div>
							<div>
								<p>Quantity</p>
							</div>
							<div className="quantity-box">
								<button className="btn btn-reduce-quantity" onClick={() => setQuantity(quantity - 1)}>-</button>
								<span>{quantity}</span>
								<button className="btn btn-add-quantity" onClick={() => setQuantity(quantity + 1)}>+</button>
							</div>
						</div>
						<div className="btns-wrapper">
							<button className="btn btn-buy-now" onClick={checkOut}>Buy Now</button>
							<button className="btn btn-add-to-cart" onClick={addToCart}>Add to Cart</button>
						</div>
					</div>
				</div>
			</div>
		</>
	)
}